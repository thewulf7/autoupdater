package io.github.thewulf7.autoupdater;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.File;
import java.util.ArrayList;

import io.github.thewulf7.autoupdater.interfaces.AutoUpdateListener;
import io.github.thewulf7.autoupdater.models.EnumErrors;
import io.github.thewulf7.autoupdater.services.DownloadService;
import io.github.thewulf7.autoupdater.utils.UpdaterUtils;
import rx.functions.Action1;

public class AutoUpdater {

    private static final String TAG = "AutoUpdater";

    private final Handler            handler;
    private       AutoUpdateListener autoUpdateListener;
    private       Context            context;
    private       String             packageName;
    private String updateFileUrl = "android.json";

    private DownloadService downloadService;
    private boolean            showNotUpdate      = false;
    private BroadcastReceiver  networkReceiver    = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!UpdaterUtils.isNetworkAvailable(context)) {
                if (downloadService != null && downloadService.getDownloadRequest() != null && !downloadService.getDownloadRequest().isCancelled()) {
                    downloadService.getDownloadRequest().cancel();
                }
            }
        }
    };

    public AutoUpdater(@NonNull Context context) {
        this.context = context;
        handler = new Handler(context.getMainLooper());

        downloadService = new DownloadService(context);
        downloadService.setDownloadFileName("update-" + UpdaterUtils.currentDate() + ".apk");
    }

    public AutoUpdater setUrl(@NonNull String url) {
        downloadService.setUri(url);
        return this;
    }

    public AutoUpdater setUpdateFileUrl(String updateFileUrl) {
        this.updateFileUrl = updateFileUrl;
        return this;
    }

    public AutoUpdater setPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public AutoUpdater setShowNotUpdate(boolean showNotUpdate) {
        this.showNotUpdate = showNotUpdate;
        return this;
    }

    public AutoUpdater setAutoUpdateListener(AutoUpdateListener autoUpdateListener) {
        this.autoUpdateListener = autoUpdateListener;
        downloadService.setAutoUpdateListener(autoUpdateListener);
        return this;
    }

    public AutoUpdater build() {
        if (Build.VERSION.SDK_INT >= 23) {
            new TedPermission(context)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            update();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        }
                    })
                    .setDeniedMessage(context.getResources().getString(R.string.autoupdater_denied_message))
                    .setDeniedCloseButtonText(android.R.string.ok)
                    .setGotoSettingButton(false)
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .check();
        } else {
            update();
        }
        return this;
    }

    public void onResume(Context context) {
        if (networkReceiver != null) {
            context.registerReceiver(networkReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }


    public void onStop(Context context) {
        if (networkReceiver != null) {
            try {
                context.unregisterReceiver(networkReceiver);
                networkReceiver = null;
            } catch (IllegalArgumentException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Download the metainfo file
     */
    private void update() {

        downloadService.downloadInfofile(updateFileUrl, new Action1<String>() {
            @Override
            public void call(final String body) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (UpdaterUtils.isUpdateAvailable(UpdaterUtils.appVersion(context), autoUpdateListener.getVersion(body))) {
                            if (BuildConfig.DEBUG) {
                                Log.v(TAG, "Update found");
                            }
                            autoUpdateListener.onUpdateFound(context, body);
                        } else if (showNotUpdate) {
                            autoUpdateListener.onUpdateNotFound(context);
                        }

                    }
                });
            }
        }, new Action1<EnumErrors>() {
            @Override
            public void call(EnumErrors enumErrors) {
                if (autoUpdateListener != null) {
                    autoUpdateListener.onFailure(enumErrors);
                }
            }
        });
    }

    public void downloadUpdates(final Context context) {
        downloadUpdates(context, packageName + ".apk");
    }

    public void downloadUpdates(final Context context, Uri url) {
        downloadUpdates(context, url.toString());
    }

    /**
     * Download the file itself
     *
     * @param context
     * @param url
     */
    public void downloadUpdates(final Context context, String url) {

        downloadService.downloadUpdateFile(url, new Action1<File>() {
            @Override
            public void call(File file) {
                if (file != null) {
                    UpdaterUtils.installApkAsFile(context, file);
                }
            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                Log.d(TAG, throwable.toString());
                if (autoUpdateListener != null) {
                    autoUpdateListener.onFailure(EnumErrors.ERROR_DOWNLOADING_UPDATES);
                }
            }
        });
    }

    private void runOnUiThread(Runnable r) {
        handler.post(r);
    }
}
