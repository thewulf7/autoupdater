package io.github.thewulf7.autoupdater.interfaces;

import android.content.Context;

import io.github.thewulf7.autoupdater.models.EnumErrors;
import io.github.thewulf7.autoupdater.models.Update;

/**
 * Created by eutkin13 on 01.02.17.
 */

public interface AutoUpdateListener {

    void onUpdateFound(Context context, String body);

    void onUpdateNotFound(Context context);

    void onDownloadStart(Context context);

    void onDownloadComplete(Context context);

    void onDownloadProgress(Context context, int progress);

    void onDownloadFailed(Context context, int errorCode, String errorMessage);

    void onFailure(EnumErrors error);

    void onFailure(Throwable throwable);

    Integer getVersion(String body);
}
