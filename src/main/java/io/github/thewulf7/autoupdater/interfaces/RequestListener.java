package io.github.thewulf7.autoupdater.interfaces;

import java.io.File;

import io.github.thewulf7.autoupdater.models.EnumErrors;
import io.github.thewulf7.autoupdater.models.Update;

public interface RequestListener {
    void onSuccess(Update update);
    void onSuccess(File file);
    void onFailure(EnumErrors error);
}
