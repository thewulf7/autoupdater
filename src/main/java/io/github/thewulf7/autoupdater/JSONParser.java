package io.github.thewulf7.autoupdater;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.github.thewulf7.autoupdater.models.Update;

public class JSONParser {

    private static final String KEY_LATEST_VERSION = "newVersion";
    private static final String KEY_RELEASE_NOTES  = "versionNotes";
    private static final String KEY_URL            = "apkUrl";

    public static Update parse(JSONObject jsonObject) {
        try {
            Update updateModel = new Update();
            updateModel.setVersion(jsonObject.getInt(KEY_LATEST_VERSION));
            updateModel.setUrl(Uri.parse(jsonObject.getString(KEY_URL)));
            JSONArray     releaseArr = jsonObject.optJSONArray(KEY_RELEASE_NOTES);
            StringBuilder builder    = new StringBuilder();
            for (int i = 0; i < releaseArr.length(); ++i) {
                builder.append(releaseArr.getString(i).trim());
                builder.append(System.getProperty("line.separator"));
            }
            updateModel.setChangelog(builder.toString());

            return updateModel;

        } catch (JSONException e) {
            Log.e("TAG", "The JSON updater file is mal-formatted.");
        }

        return null;
    }

}
