package io.github.thewulf7.autoupdater.models;

import io.github.thewulf7.autoupdater.R;

public enum EnumErrors {

    /**
     * No internet connection.
     */

    NETWORK_NOT_AVAILABLE(R.string.autoupdater_network_not_available),


    /**
     * Lost internet connection.
     */

    //NETWORK_DISCONNECTED,


    /**
     * An error occurred when downloading updates.
     */

    ERROR_DOWNLOADING_UPDATES(R.string.autoupdater_an_error_occurred_when_downloading_updates),


    /**
     * An error occurred while checking for updates.
     */

    ERROR_CHECKING_UPDATES(R.string.autoupdater_an_error_occurred_while_checking_for_updates),


    /**
     * Json file is missing.
     */

    FILE_IS_MISSING(R.string.autoupdater_metafile_is_missing),


    /**
     * The file containing information about the updates are empty.
     */

    FILE_NO_DATA(R.string.autoupdater_file_containing_information_about_updates_are_empty);

    int value;

    EnumErrors(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
