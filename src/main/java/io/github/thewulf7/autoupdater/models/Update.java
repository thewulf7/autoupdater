package io.github.thewulf7.autoupdater.models;

import android.net.Uri;

public class Update {

    private String changelog;
    private Uri url;
    private Integer version;

    public Update() {
    }

    public Update(Integer version, Uri url, String changelog) {
        this.version = version;
        this.url = url;
        this.changelog = changelog;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Uri getUrl() {
        return url;
    }

    public void setUrl(Uri url) {
        this.url = url;
    }

    public String getChangelog() {
        return changelog;
    }

    public void setChangelog(String changelog) {
        this.changelog = changelog;
    }
}
