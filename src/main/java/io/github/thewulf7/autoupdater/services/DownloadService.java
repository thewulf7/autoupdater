package io.github.thewulf7.autoupdater.services;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.ThinDownloadManager;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.github.thewulf7.autoupdater.interfaces.AutoUpdateListener;
import io.github.thewulf7.autoupdater.models.EnumErrors;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import rx.functions.Action1;

/**
 * Created by eutkin13 on 17.02.17.
 */

public class DownloadService {

    private Context            context;
    private OkHttpClient       okHttpClient;
    private String             uri;
    private String             downloadFileName;
    private AutoUpdateListener autoUpdateListener;
    private DownloadRequest    downloadRequest;

    public DownloadService(Context context) {

        this.okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        this.context = context;
    }

    public DownloadRequest getDownloadRequest() {
        return downloadRequest;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setDownloadFileName(String downloadFileName) {
        this.downloadFileName = downloadFileName;
    }

    public void setAutoUpdateListener(AutoUpdateListener autoUpdateListener) {
        this.autoUpdateListener = autoUpdateListener;
    }

    public void downloadInfofile(String path, final Action1<String> onSuccess, final Action1<EnumErrors> onError) {

        Request request = new Request.Builder()
                .url(uri + "/" + path)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Call call, IOException e) {
                onError.call(EnumErrors.FILE_NO_DATA);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    try {
                        onSuccess.call(response.body().string());
                    } catch (IOException e) {
                        onError.call(EnumErrors.ERROR_CHECKING_UPDATES);
                    }
                } else {
                    if (response.code() == 404) {
                        onError.call(EnumErrors.FILE_IS_MISSING);
                    } else {
                        onError.call(EnumErrors.ERROR_CHECKING_UPDATES);
                    }
                }
            }
        });
    }

    public void downloadUpdateFile(String path, final Action1<File> onSuccess, final Action1<Throwable> onError) {

        autoUpdateListener.onDownloadStart(context);
        Uri        downloadUri = Uri.parse(uri + "/" + path);
        final File SDCardRoot  = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download");

        if (!SDCardRoot.exists()) {
            SDCardRoot.mkdirs();
        }

        File                file            = new File(SDCardRoot, downloadFileName);
        Uri                 destinationUri  = Uri.parse(file.getPath());
        ThinDownloadManager downloadManager = new ThinDownloadManager(5);

        downloadRequest = new DownloadRequest(downloadUri)
                .setDestinationURI(destinationUri)
                .setStatusListener(new DownloadStatusListenerV1() {
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {
                        autoUpdateListener.onDownloadComplete(context);
                        onSuccess.call(new File(SDCardRoot, downloadFileName));
                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                        onError.call(new RuntimeException(context.getString(EnumErrors.ERROR_DOWNLOADING_UPDATES.getValue())));
                        autoUpdateListener.onDownloadFailed(context, errorCode, errorMessage);
                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                        autoUpdateListener.onDownloadProgress(context, progress);
                    }
                });

        downloadManager.add(downloadRequest);
    }
}
