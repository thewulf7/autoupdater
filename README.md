```
AutoUpdater autoUpdater = new AutoUpdater(getApplicationContext())
           .setUrl(BuildConfig.UPDATE_PACKAGE_URL)
           .setPackageName(BuildConfig.UPDATE_PACKAGE_NAME)
           .setAutoUpdateListener(new AutoupdateListerner())
           .setShowNotUpdate(true)
           .build();
```           
           